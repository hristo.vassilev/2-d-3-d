# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:2020-ss.1
FROM ${BASE_IMAGE}
USER root 
# install mesa
RUN apt-get update --fix-missing
RUN apt install libgl1-mesa-glx -y

USER jovyan

# Install packages via requirements.txt
ADD requirements.txt .
RUN pip install -r requirements.txt

# .. Or update conda base environment to match specifications in environment.yml
ADD ifcopenshell.yml /tmp/environment.yml

# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y
RUN jupyter labextension install jupyter-threejs
RUN jupyter labextension install jupyter-datawidgets
RUN jupyter labextension install ipycanvas

